from os import curdir, sep
from http.server import HTTPServer, SimpleHTTPRequestHandler


class HandlerServer(SimpleHTTPRequestHandler):

    def do_GET(self):
        if self.path == "/":
            self.path = "/index.html"

        try:
            response = False

            if self.path.endswith(".html"):
                media_type = 'text/html'
                response = True
            if self.path.endswith(".png"):
                media_type = 'image/png'
                response = True
            if self.path.endswith(".css"):
                media_type = 'text/css'
                response = True
            if self.path.endswith(".js"):
                media_type = 'text/js'
                response = True
            if self.path.endswith(".woff2"):
                media_type = 'font/woff2'
                response = True
            if self.path.endswith(".eot"):
                media_type = 'font/eot'
                response = True
            if self.path.endswith(".svg"):
                media_type = 'image/svg+xml'
                response = True
            if self.path.endswith(".ttf"):
                media_type = 'font/ttf'
                response = True
            if self.path.endswith(".woff"):
                media_type = 'font/woff'
                response = True

            if response:
                open_page = open(curdir + sep + self.path, 'rb')
                self.send_response(200)
                self.send_header('Content-type', media_type)
                self.end_headers()
                self.wfile.write(open_page.read())
                open_page.close()

            return

        except OSError:
            open_page = open(curdir + sep + 'error.html', 'rb')
            self.send_response(404)
            self.send_header('Content-type', media_type)
            self.end_headers()
            self.wfile.write(open_page.read())
            open_page.close()


def run_server(handler_server, port=8080):

    server_url = 'http://localhost'

    try:
        server = HTTPServer(('', port), handler_server)
        print('Serving at ' + server_url + ':' + str(port))
        server.serve_forever()

    except KeyboardInterrupt:
        print('Shutting down server')
        server.socket.close()


run_server(HandlerServer)

